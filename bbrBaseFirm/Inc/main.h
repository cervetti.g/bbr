/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BAT_Pin GPIO_PIN_1
#define BAT_GPIO_Port GPIOA
#define GY91_SCK_Pin GPIO_PIN_5
#define GY91_SCK_GPIO_Port GPIOA
#define GY91_SDO_Pin GPIO_PIN_6
#define GY91_SDO_GPIO_Port GPIOA
#define GY91_SDI_Pin GPIO_PIN_7
#define GY91_SDI_GPIO_Port GPIOA
#define LED_AUX_Pin GPIO_PIN_0
#define LED_AUX_GPIO_Port GPIOB
#define HC05_KEY_Pin GPIO_PIN_1
#define HC05_KEY_GPIO_Port GPIOB
#define GY91_CSB_Pin GPIO_PIN_8
#define GY91_CSB_GPIO_Port GPIOA
#define HC05_Tx_Pin GPIO_PIN_9
#define HC05_Tx_GPIO_Port GPIOA
#define HC05_Rx_Pin GPIO_PIN_10
#define HC05_Rx_GPIO_Port GPIOA
#define GY91_NCS_Pin GPIO_PIN_11
#define GY91_NCS_GPIO_Port GPIOA
#define A4988_EN2_Pin GPIO_PIN_12
#define A4988_EN2_GPIO_Port GPIOA
#define A4988_STP2_Pin GPIO_PIN_15
#define A4988_STP2_GPIO_Port GPIOA
#define A4988_DIR2_Pin GPIO_PIN_3
#define A4988_DIR2_GPIO_Port GPIOB
#define A4988_EN3_Pin GPIO_PIN_4
#define A4988_EN3_GPIO_Port GPIOB
#define A4988_STP3_Pin GPIO_PIN_5
#define A4988_STP3_GPIO_Port GPIOB
#define A4988_DIR3_Pin GPIO_PIN_6
#define A4988_DIR3_GPIO_Port GPIOB
#define A4988_EN1_Pin GPIO_PIN_7
#define A4988_EN1_GPIO_Port GPIOB
#define A4988_STP1_Pin GPIO_PIN_8
#define A4988_STP1_GPIO_Port GPIOB
#define A4988_DIR1_Pin GPIO_PIN_9
#define A4988_DIR1_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
