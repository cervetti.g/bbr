#ifndef MICRO_CLI_CONFIG
#define MICRO_CLI_CONFIG
#define MICRO_CLI_LIB_VER "0.1"

// Prints in console each character received too.
#define COMAND_ECHO

/*
Define you prompt string here. You can use colors escape code, for highlight you prompt,
for example this prompt will green color (if you terminal supports color)*/
#define _PROMPT_DEFAULT "\e[32mbbr: \e[37m"	// green color  ""

//Buffer size for storing commands.
#define CLI_BUFFER_SIZE 40

//Defines the separator used for each command.
#define WORD_SEPARATOR " "

/*
New line symbol */
#define _ENDL_LF

#if defined(_ENDL_CR)
#define ENDL "\r"
#elif defined(_ENDL_CRLF)
#define ENDL "\r\n"
#elif defined(_ENDL_LF)
#define ENDL "\n"
#elif defined(_ENDL_LFCR)
#define ENDL "\n\r"
#else
#error "You must define new line symbol."
#endif

#endif