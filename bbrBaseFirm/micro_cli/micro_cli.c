#include "micro_cli.h"
/**
 * @brief this function gets the current number of words sent in a command
 * Each word is separated by 
 **/

int strip_command(const char* command)
{
    int count = 0;
    char stripBuffer[CLI_BUFFER_SIZE];

    char *token = strcpy((char *)&stripBuffer, command);

    /* walk through other tokens */
    while (strtok_r(token, WORD_SEPARATOR, &token) != NULL)
    {
        ++count;
    }

    return count;
}

void cli_init(cli_instance *cli, void (*print)(char *))
{
    //Reserve memory for the buffer.
    cli->bufferPtr = malloc(CLI_BUFFER_SIZE);
    //Set values to 0.
    memset(cli->bufferPtr, 0, CLI_BUFFER_SIZE);
    //Save the print function
    cli->print = print;
    //Point the second pointer to the same amount of data
    cli->bufferIndex = cli->bufferPtr;
}

void cli_insert_char(cli_instance *cli, char ch)
{
#ifdef COMAND_ECHO
    char charTemp[30];
#endif
    if (ch == KEY_HT)
    {
        if (cli->tabulation != NULL)
        {
            uint8_t commandCount = strip_command((const char*)cli->bufferPtr);
            cli->tabulation(commandCount, cli->bufferPtr);
        }
    }
    else if ((ch == KEY_BS))
    {
        //If Backspace was pressed and it is
        //not the first character pressed.
        if (cli->bufferIndex > cli->bufferPtr){
            //move the cursor to the previous character.
            --cli->bufferIndex;
#ifdef COMAND_ECHO
            charTemp[0] = ch;
            charTemp[1] = '\0';
            cli->print((char *)&charTemp);
#endif
        }
    }
    else
    {
        //Insert new character to the buffer.
        *cli->bufferIndex = ch;
        ++cli->bufferIndex;
#ifdef COMAND_ECHO
        charTemp[0] = ch;
        charTemp[1] = '\0';
        cli->print((char *)&charTemp);
#endif
    }
    //Checks if there is a ENDL command typed.
    char *endLine = strstr((const char *)cli->bufferPtr, ENDL);
    if (endLine != NULL)
    {
        *endLine = '\0';
        //Call the executor to process the command
        int commandCount = strip_command((const char*)cli->bufferPtr);
        int result = cli->execute(commandCount, cli->bufferPtr);
        //If the command was executed correctly.
        if (result == 1)
        {
            //Reset the buffer and point the index pointer to the first position of the
            //buffer.
            memset(cli->bufferPtr, 0, CLI_BUFFER_SIZE);
            cli->bufferIndex = cli->bufferPtr;
            //Print the new line command.
            cli->print(_PROMPT_DEFAULT);
        }
    }
}

void cli_set_execute_callback(cli_instance *cli, int (*execute)(int, char *))
{
    if (execute != NULL)
        cli->execute = execute;
}

void cli_set_execute_tab(cli_instance *cli, int (*tab_exec)(int, char *))
{
    if (tab_exec != NULL)
        cli->tabulation = tab_exec;
}