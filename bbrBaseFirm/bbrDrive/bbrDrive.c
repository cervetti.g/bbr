/**
 * @file bbrDrive.c
 * @author Gonzalo Cervetti (cervetti.g@gmail.com)
 * @brief Calculates the behaviour of the motors in order to move a BBR drive system.
 * @version 0.1
 * @date 2019-11-30
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "../bbrDrive/bbrDrive.h"

/* 
* Matrix calculation constants
*/
void bbrDrive_init(a4988Handler *motor1_, a4988Handler *motor2_,
				   a4988Handler *motor3_)
{
	//Copy the references to the internal variables
	
	m1.m = motor1_;
	m2.m = motor2_;
	m3.m = motor3_;

	//Set microsteps
	a4988_setMicroStep(m1.m, FULL);
	a4988_setMicroStep(m2.m, FULL);
	a4988_setMicroStep(m3.m, FULL);

	//Initialize matrix
	Wval[0] = 0;
	Wval[1] = cos(THETA);
	Wval[2] = -sin(THETA);
	Wval[3] = -((M_SQRT3 / 2)) * cos(THETA);
	Wval[4] = (-(1 / 2)) * cos(THETA);
	Wval[5] = -sin(THETA);
	Wval[6] = ((M_SQRT3 / 2)) * cos(THETA);
	Wval[7] = (-(1 / 2)) * cos(THETA);
	Wval[8] = -sin(THETA);

	Rval[0] = 0;
	Rval[1] = -1;
	Rval[2] = 0;
	Rval[3] = 1;
	Rval[4] = 0;
	Rval[5] = 0;
	Rval[6] = 0;
	Rval[7] = 0;
	Rval[8] = -BALL_RADIUS;

	RTvalues[0] = 0;
	RTvalues[1] = 1;
	RTvalues[2] = 0;
	RTvalues[3] = -1;
	RTvalues[4] = 0;
	RTvalues[5] = 0;
	RTvalues[6] = 0;
	RTvalues[7] = 0;
	RTvalues[8] = -BALL_RADIUS;

	//Calculates the max acceleration allowed in each interrupt reload value of the micro controller. 
	m1.DelayAcceleration = calc_speed(MAX_ACCELERATION/ACCELERATION_UPDATE_FREQ, m1.m->stepsPerTurn, m1.m->microstp);
	m2.DelayAcceleration = calc_speed(MAX_ACCELERATION/ACCELERATION_UPDATE_FREQ, m2.m->stepsPerTurn, m2.m->microstp);
	m3.DelayAcceleration = calc_speed(MAX_ACCELERATION/ACCELERATION_UPDATE_FREQ, m3.m->stepsPerTurn, m3.m->microstp);
}

void bbrDrive_move(float vx, float vy, float wbz)
{
	float32_t rSwMatBuff[9];
	float32_t rMrtMatBuff[9];
	float32_t rrtMrMatBuff[9];
	float32_t rrtrMvMatBuff[3];
	
	//Variable declaration
	float vVal[] = {
		vx,
		vy,
		wbz,
	};
	arm_matrix_instance_f32 rMat;
	arm_matrix_instance_f32 rTMat;
	arm_matrix_instance_f32 wMat;
	arm_matrix_instance_f32 vMat;
	arm_matrix_instance_f32 rSwMat;		//Rotational matrix scaled wheel radius.
	arm_matrix_instance_f32 rMrtMat;   	//(Rotational matrix scaled wheel radius) X (R').
	arm_matrix_instance_f32 rrtMrMat;  	//((Rotational matrix scaled wheel radius) x (R')) x (R).
	arm_matrix_instance_f32 rrtrMvMat; 	//(((Rotational matrix scaled wheel radius) x (R')) x (R)) X V.

	//matrix initialization
	arm_mat_init_f32(&rMat, 3, 3, Rval);
	arm_mat_init_f32(&rTMat, 3, 3, RTvalues);
	arm_mat_init_f32(&wMat, 3, 3, Wval);
	arm_mat_init_f32(&vMat, 3, 1, vVal);
	arm_mat_init_f32(&rSwMat, 3, 3, rSwMatBuff);
	arm_mat_init_f32(&rMrtMat, 3, 3, rMrtMatBuff);
	arm_mat_init_f32(&rrtMrMat, 3, 3, rrtMrMatBuff);
	arm_mat_init_f32(&rrtrMvMat, 3, 1, rrtrMvMatBuff);

	//Speed calculation
	arm_mat_scale_f32(&wMat, (-1 / WHEEL_RADIUS), &rSwMat);
	arm_mat_mult_f32(&rSwMat, &rTMat, &rMrtMat);
	arm_mat_mult_f32(&rMrtMat, &rMat, &rrtMrMat);
	arm_mat_mult_f32(&rrtMrMat, &vMat, &rrtrMvMat);

	//Sets the direction of the motor
	if(rrtrMvMat.pData[0] >= 0) HAL_GPIO_WritePin(m1.m->DIR_PORT, m1.m->DIR_PIN, GPIO_PIN_SET);
	else HAL_GPIO_WritePin(m1.m->DIR_PORT, m1.m->DIR_PIN, GPIO_PIN_RESET);
	if(rrtrMvMat.pData[1] >= 0) HAL_GPIO_WritePin(m2.m->DIR_PORT, m2.m->DIR_PIN, GPIO_PIN_SET);
	else HAL_GPIO_WritePin(m2.m->DIR_PORT, m2.m->DIR_PIN, GPIO_PIN_RESET);
	if(rrtrMvMat.pData[2] >= 0) HAL_GPIO_WritePin(m3.m->DIR_PORT, m3.m->DIR_PIN, GPIO_PIN_SET);
	else HAL_GPIO_WritePin(m3.m->DIR_PORT, m3.m->DIR_PIN, GPIO_PIN_RESET);

	//Calculate the how many Timer ticks has to count in order 
	//to reach the desired frequency. 
	if(rrtrMvMat.pData[0] == 0.0){
		m1.DelayTarget = 0;
		m1.DelayCurrent = 0;
		m1.DelayCount = 0;
		m1.moving = false;
		HAL_GPIO_WritePin(m1.m->STEP_PORT,m1.m->STEP_PIN, GPIO_PIN_RESET);
	}
	else
	{
		//Calculates the delay to reach the target speed.
		m1.DelayTarget = calc_speed(fabs(rrtrMvMat.pData[0]), m1.m->stepsPerTurn,
								   (uint32_t)m1.m->microstp);
		//If this delay is higher than the max allowed delay acceleration, then apply it on the first step. 
		if(m1.DelayCurrent == 0) m1.DelayCurrent = 15.0; 
		m1.DelayCount = (int32_t)m1.DelayCurrent;
		m1.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
		m1.moving = true;
	}
	if(rrtrMvMat.pData[1] == 0.0){
		m2.DelayTarget = 0;
		m2.DelayCurrent = 0;
		m2.DelayCount = 0;
		m2.moving = false;
		HAL_GPIO_WritePin(m2.m->STEP_PORT,m2.m->STEP_PIN, GPIO_PIN_RESET);
	}
	else
	{
		//Calculates the delay to reach the target speed.
		m2.DelayTarget = calc_speed(fabs(rrtrMvMat.pData[1]), m2.m->stepsPerTurn,
								   (uint32_t)m2.m->microstp);
		//If this delay is higher than the max allowed delay acceleration, then apply it on the first step. 
		if(m2.DelayCurrent == 0) m2.DelayCurrent = 15.0; 
		m2.DelayCount = (int32_t)m2.DelayCurrent;
		m2.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
		m2.moving = true;
	}
	if(rrtrMvMat.pData[0] == 0.0){
		m3.DelayTarget = 0;
		m3.DelayCurrent = 0;
		m3.DelayCount = 0;
		m3.moving = false;
		HAL_GPIO_WritePin(m3.m->STEP_PORT,m3.m->STEP_PIN, GPIO_PIN_RESET);
	}
	else
	{
		//Calculates the delay to reach the target speed.
		m3.DelayTarget = calc_speed(fabs(rrtrMvMat.pData[0]), m3.m->stepsPerTurn,
								   (uint32_t)m3.m->microstp);
		//If this delay is higher than the max allowed delay acceleration, then apply it on the first step. 
		if(m3.DelayCurrent == 0) m3.DelayCurrent = 15.0; 
		m3.DelayCount = (int32_t)m3.DelayCurrent;
		m3.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
		m3.moving = true;
	}
}

void bbrDrive_moveSimple_1(float speed){
	if(speed == 0.0){
		m1.DelayTarget = 0;
		m1.DelayCurrent = 0;
		m1.DelayCount = 0;
		m1.moving = false;
		HAL_GPIO_WritePin(m1.m->STEP_PORT,m1.m->STEP_PIN, GPIO_PIN_RESET);
	}
	else
	{
		//Calculates the delay to reach the target speed.
		m1.DelayTarget = calc_speed(fabs(speed), m1.m->stepsPerTurn,
								   (uint32_t)m1.m->microstp);
		//If this delay is higher than the max allowed delay acceleration, then apply it on the first step. 
		if(m1.DelayCurrent == 0) m1.DelayCurrent = 15.0; 
		m1.DelayCount = (int32_t)m1.DelayCurrent;
		m1.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
		m1.moving = true;
	}
}

void bbrDrive_moveSimple_2(float speed){
	if(speed == 0.0){
		m2.DelayTarget = 0;
		m2.DelayCurrent = 0;
		m2.DelayCount = 0;
		m2.moving = false;
		HAL_GPIO_WritePin(m2.m->STEP_PORT,m2.m->STEP_PIN, GPIO_PIN_RESET);
	}
	else
	{
		//Calculates the delay to reach the target speed.
		m2.DelayTarget = calc_speed(fabs(speed), m2.m->stepsPerTurn,
								   (uint32_t)m2.m->microstp);
		//If this delay is higher than the max allowed delay acceleration, then apply it on the first step. 
		if(m2.DelayCurrent == 0) m2.DelayCurrent = 15.0; 
		m2.DelayCount = (int32_t)m2.DelayCurrent;
		m2.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
		m2.moving = true;
	}
}

void bbrDrive_moveSimple_3(float speed){
	if(speed == 0.0){
		m3.DelayTarget = 0;
		m3.DelayCurrent = 0;
		m3.DelayCount = 0;
		m3.moving = false;
		HAL_GPIO_WritePin(m3.m->STEP_PORT,m3.m->STEP_PIN, GPIO_PIN_RESET);
	}
	else
	{
		//Calculates the delay to reach the target speed.
		m3.DelayTarget = calc_speed(fabs(speed), m3.m->stepsPerTurn,
								   (uint32_t)m3.m->microstp);
		//If this delay is higher than the max allowed delay acceleration, then apply it on the first step. 
		if(m3.DelayCurrent == 0) m3.DelayCurrent = 15.0; 
		m3.DelayCount = (int32_t)m3.DelayCurrent;
		m3.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
		m3.moving = true;
	}
}

void updateDrive()
{
	if ((m1.moving != false))
	{
		//This part of the code manages the acceleration of the signal. 
		--m1.AccelerationCount;
		if(m1.AccelerationCount == 0){
			//Reload the count of the for the nex check of the acceleration. 
			m1.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
			//If the DelayTarget is higher than the DelayCurrent, it has to increment DelayCurrent(reduce the speed)
			if(m1.DelayTarget > m1.DelayCurrent){
				m1.DelayCurrent =  m1.DelayTarget;
			}
			//If the DelayTarget is lower than the DelayCurrent, it has to decrement DelayCurrent(increment the speed)
			else if(m1.DelayTarget < m1.DelayCurrent){
				if((m1.DelayCurrent - m1.DelayTarget) > m1.DelayAcceleration) {
					m1.DelayCurrent =  m1.DelayTarget;
				}
				else {
					m1.DelayCurrent = calc_acceleration_step(m1.DelayCurrent,m1.DelayAcceleration,1);
				}
			}
		}

		//This part of the code is in charge of toogling the step pin at a desired speed.
		--m1.DelayCount;
		if (m1.DelayCount == 0)
		{
			//For all the cases, it has to reload the DelayCount to the value set in DelayCurrent
			HAL_GPIO_TogglePin(m1.m->STEP_PORT, m1.m->STEP_PIN);
			m1.DelayCount = (int32_t)m1.DelayCurrent;
		}		
	}

	if ((m2.moving != false))
	{
		//This part of the code manages the acceleration of the signal. 
		--m2.AccelerationCount;
		if(m2.AccelerationCount == 0){
			//Reload the count of the for the nex check of the acceleration. 
			m2.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
			//If the DelayTarget is higher than the DelayCurrent, it has to increment DelayCurrent(reduce the speed)
			if(m2.DelayTarget > m2.DelayCurrent){
				m2.DelayCurrent =  m2.DelayTarget;
			}
			//If the DelayTarget is lower than the DelayCurrent, it has to decrement DelayCurrent(increment the speed)
			else if(m2.DelayTarget < m2.DelayCurrent){
				if((m2.DelayCurrent - m2.DelayTarget) > m2.DelayAcceleration) {
					m2.DelayCurrent =  m2.DelayTarget;
				}
				else {
					m2.DelayCurrent = calc_acceleration_step(m2.DelayCurrent,m2.DelayAcceleration,1);
				}
			}
		}

		//This part of the code is in charge of toogling the step pin at a desired speed.
		--m2.DelayCount;
		if (m2.DelayCount == 0)
		{
			//For all the cases, it has to reload the DelayCount to the value set in DelayCurrent
			HAL_GPIO_TogglePin(m2.m->STEP_PORT, m2.m->STEP_PIN);
			m2.DelayCount = (int32_t)m2.DelayCurrent;
		}			
	}
	
	if ((m3.moving != false))
	{
		//This part of the code manages the acceleration of the signal. 
		--m3.AccelerationCount;
		if(m3.AccelerationCount == 0){
			//Reload the count of the for the nex check of the acceleration. 
			m3.AccelerationCount = ACCELERATION_COUNT_RELOAD_VALUE;
			//If the DelayTarget is higher than the DelayCurrent, it has to increment DelayCurrent(reduce the speed)
			if(m3.DelayTarget > m3.DelayCurrent){
				m3.DelayCurrent =  m3.DelayTarget;
			}
			//If the DelayTarget is lower than the DelayCurrent, it has to decrement DelayCurrent(increment the speed)
			else if(m3.DelayTarget < m3.DelayCurrent){
				if((m3.DelayCurrent - m3.DelayTarget) > m3.DelayAcceleration) {
					m3.DelayCurrent =  m3.DelayTarget;
				}
				else {
					m3.DelayCurrent = calc_acceleration_step(m3.DelayCurrent,m3.DelayAcceleration,1);
				}
			}
		}

		//This part of the code is in charge of toogling the step pin at a desired speed.
		--m3.DelayCount;
		if (m3.DelayCount == 0)
		{
			//For all the cases, it has to reload the DelayCount to the value set in DelayCurrent
			HAL_GPIO_TogglePin(m3.m->STEP_PORT, m3.m->STEP_PIN);
			m3.DelayCount = (int32_t)m3.DelayCurrent;
		}		
	}
}

void bbrDrive_setStatus(driverStatus status){
	a4988_setStatus(m1.m, status);
	a4988_setStatus(m2.m, status);
	a4988_setStatus(m3.m, status);
}

//Transform the desired angular speed[rad/seg] into tick counts.
float32_t calc_speed(float speed, float stepsPerTurn, uint32_t microstep)
{
	return ((TIMER_INTERRPT_FREQ / ((speed * (stepsPerTurn * (float32_t)microstep)) / (2*M_PI))) / 2);
}

//Calculates the next speed count after applying 'acceleration' during 'time' amount of time. 
float32_t calc_acceleration_step(float32_t speed, float32_t acceleration, int32_t time){
	return (1/((1/speed) + ((1/acceleration)* (float32_t)time)));
}
