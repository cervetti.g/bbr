/**
 * bbrDrive.h
 *
 *  Created on: Oct 1, 2019
 *      Author: Gonza
 *
 * Esta librería se basa en la librería A4988 para generar el movimiento del robot. En este caso utilizamos un movimiento
 * basado en 3 motores. Tener en cuenta que la librería sólo se encarga de mover los motores. El control PID se encuentra en el programa
 * principal. La dirección del movimiento se basa En la ilustración de abajo.
 *
 *      Convensión de los motores:
 *                             				Motor 1

                                       ````````````````````
                                       +::::::::::::::::::+                         ^
                                       +------------------+                        / \
                                       +------------------+                         |
                                       +------------------+                         |
                                       +::::::::::::::::::+
                                       `````````-/`````````                     Adelante
                                         `-.....-:......-
                                         .-`..........``:
                                      ``````````````````..``
                                   ``.``````````````````````.``
                                  ```````````````````````````````
                                `.``````````````````````````````.`
                               `.``````````````````````````````````
                              `.````````````````````````````````````
                             `.````````````````````````````````````.`
                             .``````````Visto desde arriba  ````````.
                             .``````````````````````````````````````.
                             .``````````````````````````````````````.
                             .``````````````````````````````````````-`
                     `..   ..-.````````````````````````````````````.....`   -.`
                 ``-///+- `:.`.````````````````````````````````````.```-. `+///:.`
              `.://:----+: `-.`.``````````````````````````````````.```-. `+:---:///-`
              :o:--------+/  -.`.````````````````````````````````.```-` .+:--------/o`
               :+---------/+--:.`..````````````````````````````..```:-.:+:--------:+`
                -+:--------/+` .-``..````````````````````````..````-` -+:--------:+`
                 .o:--------:+. `-``.-..``````````````````.``.-.`.-  :+---------/+
                  `o:--------:+. `..    ````.````````.````     `..  //---------+/
                   `+:---------+-              ````               `+/---------+:
 +``+``.`/``. .`-:.  +/------:///                                 .+/:------:o- -: -: .`.-`.``..-:
 +  Motor 2           /+--://:.                                     `-///:-:o.  :/ Motor 3
 -.-.`-:.-.::`-`/-`    :+/-`                                            .:/o`   .`/`-.:-`-.:-.`.--

 */

#ifndef BBRDRIVE_BBRDRIVE_H_
#define BBRDRIVE_BBRDRIVE_H_
#include "../A4988/a4988.h"
#include "../dwt_delay/dwt_delay.h"
#include "arm_math.h"
#include "math.h"

/// @brief Radius of the ball.
static const float32_t BALL_RADIUS = 0.10;

/// @brief Angle between the axis of each wheel and the base plane of the robot (40°).
static const float32_t THETA = ((2.0/9.0)*M_PI);

/// @brief Wheel radius in [m].
static const float32_t WHEEL_RADIUS = 0.035;

/// @brief Interrupt period of the timer that sets the time base for steps. It must be set in Seg [S].
static const float32_t TIMER_INTERRPT_FREQ = 10000;

/// @brief Max motor speed of each motor [rads/seg].
static const float32_t MAX_SPEED = 22 * PI;

/// @brief Max motor speed of each motor [rads/seg²].
static const float32_t MAX_ACCELERATION = 2 * PI;

/// @brief Frequency at which the acceleration modifies the speed of the motor. 
static const float32_t ACCELERATION_UPDATE_FREQ = 2;

/// @brief Internarl value for acceleration count. DO NOT TOUCH.
static const float32_t ACCELERATION_COUNT_RELOAD_VALUE =  TIMER_INTERRPT_FREQ / ACCELERATION_UPDATE_FREQ;

/*!
 * @enum motorDrive
 * @brief Stores all the information related to the motor that is being controlled
 */
typedef struct
{
   a4988Handler *m;           ///< Motor Handler.
   float speed;               ///< Current speed of each motor
   float Acceleration;        ///< Current acceleration of each motor
   float32_t DelayCurrent;      ///< Stores at how many timer interrupts the position of each motor should change. This sets the speed of the motor.
   int32_t DelayCount;         ///< Current delay that sets the current speed of the motor. 
   float32_t DelayTarget;       ///< Value that will be copied when DelayCount reaches to 0.
   int32_t DelayAcceleration; ///< Max increment step allowed to increment/Decrement acceleration. 
   int32_t AccelerationCount; 
   bool moving;      	      ///< Sets if the motor has to move and the direcction of the movement.
} motorDrive;

float32_t Rval[9];

float32_t RTvalues[9];

float32_t Wval[9];

/// @brief Front motor handler.
 motorDrive m1; 
/// @brief Left motor handler.
 motorDrive m2; 
/// @brief Right motor handler.
 motorDrive m3; 

/**
 *
 * @param motor1
 * @param motor2
 * @param motor3
 */
void bbrDrive_init(a4988Handler *motor1, a4988Handler *motor2, a4988Handler *motor3);

/**
 *
 * @param vx
 * @param vy
 * @param wbz
 */
void bbrDrive_move(float vx, float vy, float wbz);

/**
 * @brief moves one of the motors at a desired speed
 * 
 * @param speed speed of the motor in [rad/seg]
 */
void bbrDrive_moveSimple_1(float speed);
void bbrDrive_moveSimple_2(float speed);
void bbrDrive_moveSimple_3(float speed);

/**
 * @brief call this function in order to update the position of the motors.
 */
void updateDrive();

/**
 * @brief Enables or disables all the motors.
 * @param status ACTIVE or OFF.
 */
void bbrDrive_setStatus(driverStatus status);

//Transform the desired angular speed[rad/seg] into interrupt count.
float32_t calc_speed(float speed, float stepsPerTurn, uint32_t microstep);

//Calculates the next speed count after applying 'acceleration' during 'time' amount of time. 
float32_t calc_acceleration_step(float32_t speed, float32_t acceleration, int32_t time);
#endif /* BBRDRIVE_BBRDRIVE_H_ */
