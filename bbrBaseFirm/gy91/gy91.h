/**
 * @file gy91.h
 * @author Gonzalo Cervetti (cervetti.g@gmail.com)
 * @brief This library manages the GY-91 sensor board
 * @version 0.1
 * @date 2019-11-30
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#ifndef _GY91_H_
#define _GY91_H_

#include "spi.h"
#include "gpio.h"

#include "stdbool.h"

#include "config.h"
#include "registerMap.h"


/**
 * @brief Initialization of GY91 module
 * 
 * @param spiInstance SPI instance to communicate with the device.
 * @param ncsPort Port on which NCS is connected.
 * @param ncsPin NCS pin.
 * @param csbPort Port on which CSB is connected.
 * @param csbPin CSB pin.
 * @return true When initialization is correct and the device was found.
 * @return false When there is an error in the device initilization. 
 */
bool gy91_init(SPI_HandleTypeDef *spiInstance, GPIO_TypeDef ncsPort, uint16_t ncsPin, GPIO_TypeDef csbPort, uint16_t csbPin);

/// @brief SPI instance to communicate with the device. 
SPI_HandleTypeDef *hspi;

/// @brief port of the pin NCS.
GPIO_TypeDef ncsPort;

/// @brief Pin number of ncs
uint16_t ncsPin;

/// @brief port of the pin CSB.
GPIO_TypeDef csbPort;

/// @brief Pin number of CSB.
uint16_t csbPin;

#endif