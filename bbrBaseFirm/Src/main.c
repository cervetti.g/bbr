/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define ARM_MATH_CM3
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../micro_cli/micro_cli.h"
#include "../micro_cli/config.h"
#include "../bbrDrive/bbrDrive.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define COMMAND_SEPARATOR "="
#define COMMAND_ECHO "echo"
#define COMMAND_MOVE "move"
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//Buffer that stores each value comming.
uint8_t buffer[1];
const char *commandTable[] = {
    "echo",
    "move",
    "xmove",
    "ymove",
    "zmove",
    "1move",
    "2move",
    "3move",
    "stop",
};
float vx, vy, vwz;
a4988Handler mot1;
a4988Handler mot2;
a4988Handler mot3;
// create microrl object and pointer on it
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void console_print(char *message);
int console_execute(int argc, char *argv);
int tab_execute(int argc, char *argv);
cli_instance mCli;

bool newDataCount = false;
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  cli_init(&mCli, &console_print);
  cli_set_execute_callback(&mCli, &console_execute);
  cli_set_execute_tab(&mCli, tab_execute);
  HAL_UART_Receive_IT(&huart1, (uint8_t *)&buffer, 1);
  a4988_init(&mot1, A4988_EN1_GPIO_Port, A4988_EN1_Pin, A4988_STP1_GPIO_Port, A4988_STP1_Pin, A4988_DIR1_GPIO_Port, A4988_DIR1_Pin);
  a4988_init(&mot2, A4988_EN2_GPIO_Port, A4988_EN2_Pin, A4988_STP2_GPIO_Port, A4988_STP2_Pin, A4988_DIR2_GPIO_Port, A4988_DIR2_Pin);
  a4988_init(&mot3, A4988_EN3_GPIO_Port, A4988_EN3_Pin, A4988_STP3_GPIO_Port, A4988_STP3_Pin, A4988_DIR3_GPIO_Port, A4988_DIR3_Pin);
  bbrDrive_init(&mot1, &mot2, &mot3);
  bbrDrive_setStatus(ACTIVE);
  vx = vy = vwz = 0.0;
  bbrDrive_move(0.0,0.0,0.0);
  HAL_TIM_Base_Start_IT(&htim2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    if (newDataCount == true)
    {
      //Put a character in the terminal.
      cli_insert_char(&mCli, buffer[0]);
      //Reset the flag.
      newDataCount = false;
    }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV8;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

//This function is being called every time a char is received in UART.
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if (huart->Instance == USART1)
  {
    //Alert that there is new data to check.
    newDataCount = true;
    //Wait again for another value sent.
    HAL_UART_Receive_IT(&huart1, (uint8_t *)&buffer, 1);
  }
}

// print to stream callback
void console_print(char *message)
{
  //Get the lenght of the string
  uint16_t nbytes = strlen((const char *)message);
  //Store the string into a new array
  char *msg = malloc(nbytes);
  strcpy(msg, (const char *)message);
  //print the string into the console.
  HAL_UART_Transmit_IT(&huart1, (uint8_t *)msg, nbytes);
  //Free the resources.
  free(msg);
  //Delay to let the data to be sent.
  HAL_Delay(20);
}

//Execute console commands
int console_execute(int argc, char *argv)
{
  char *command = strtok(argv, WORD_SEPARATOR);
  //This command sends a simple message to check everything is working.
  if (strcmp(command, commandTable[0]) == 0)
  {
    console_print("BBR base v1.0\n");
  }
  //Command for moving the robot without stabilization control.
  else if (strcmp(command, commandTable[1]) == 0)
  {
    //While there are parameters available, get them one by one.
    char *param;
    while ((param = strtok(NULL, WORD_SEPARATOR)) != NULL)
    {
      if ((*param == 'v') && (*(param + 2) == '='))
      {
        ++param;
        switch (*param)
        {
        case 'x':
          param += 2;
          vx = atof(param);
          console_print("inserted vx\n");
          break;
        case 'y':
          param += 2;
          vy = atof(param);
          console_print("inserted vy\n");
          break;
        case 'z':
          param += 2;
          vwz = atof(param);
          console_print("inserted vz\n");
          break;
        }
      }
      else
      {
        console_print("Wrong param format\n");
      }
    }
  }
  else if (strcmp(command, commandTable[2]) == 0){
    bbrDrive_move(0.50,0.0,0.0);
  }
  else if (strcmp(command, commandTable[3]) == 0){
    bbrDrive_move(0.0,0.5,0.0);
  }
  else if (strcmp(command, commandTable[4]) == 0){
    bbrDrive_move(0.0,0.0,0.50);
  }
  else if (strcmp(command, commandTable[5]) == 0){
    char *param;
    if ((param = strtok(NULL, WORD_SEPARATOR)) != NULL){
      float v = atof(param);
      bbrDrive_moveSimple_1(v);
    }
    else {
      bbrDrive_moveSimple_1(0.0);
    }
  }
  else if (strcmp(command, commandTable[6]) == 0){
    char *param;
    if ((param = strtok(NULL, WORD_SEPARATOR)) != NULL){
      float v = atof(param);
      bbrDrive_moveSimple_2(v);
    }
    else {
      bbrDrive_moveSimple_2(0.0);
    }
  }
  else if (strcmp(command, commandTable[7]) == 0){
    char *param;
    if ((param = strtok(NULL, WORD_SEPARATOR)) != NULL){
      float v = atof(param);
      bbrDrive_moveSimple_3(v);
    }
    else {
      bbrDrive_moveSimple_3(0.0);
    }
  }
  else if (strcmp(command, commandTable[8]) == 0){
    bbrDrive_move(0.0,0.0,0.0);
  }
  //Return 1 when no command was executed.
  return 1;
}

//This function is called when tab is pressed.
int tab_execute(int argc, char *argv)
{
  //Get how many commands do we have.
  uint8_t commandCount = (sizeof(commandTable) / sizeof(char *));
  uint8_t count;
  uint8_t ocurrences = 0;
  char *lastOcurrence = NULL;
  char *command = strtok(argv, WORD_SEPARATOR);
  //Get the lenght of the command typed so far.
  uint8_t commandLenght = strlen(command);
  //Compares each listed command with the buttons pressed.
  for (count = 0; count < commandCount; ++count)
  {
    //This is done because it has to be compared from the beginning up to the lenght of the command typed.
    int cmp = strncmp(commandTable[count], command, commandLenght);
    if (cmp == 0)
    {
      //Increments the number of possible commands found.
      ++ocurrences;
      //Stores the last command that matched.
      lastOcurrence = strstr(commandTable[count], command);
      //Jump the position that points the pointer up to the position that has to be completed.
      lastOcurrence += commandLenght;
    }
  }
  //If there is just one possible command.
  if (ocurrences == 1)
  {
    //autocomplete the command.
    console_print(lastOcurrence);
    //Calculates of many commands has the autocomplete.
    uint16_t charsToMove = strlen(lastOcurrence);
    //Moves the autocomplete to the buffer of the console.
    strcat(mCli.bufferIndex, lastOcurrence);
    //Increments the position of the pointer to the last part of the message, so new data can be inserted.
    mCli.bufferIndex += charsToMove;
  }
  //If there is more than one possibility.
  else if (ocurrences > 1)
  {
    //Prints enter.
    console_print("\n");
    //Prints all the possible commands.
    for (count = 0; count < commandCount; ++count)
    {
      int cmp = strncmp(commandTable[count], command, commandLenght);
      if (cmp >= 0)
      {
        console_print((char *)commandTable[count]);
        console_print("\n");
      }
    }
    //Prints the prompt again with the letters pressed at the moment.
    console_print(_PROMPT_DEFAULT);
    console_print(command);
  }
  return 1;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
