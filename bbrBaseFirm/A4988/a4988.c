#include	"a4988.h"
#include 	"../dwt_delay/dwt_delay.h"
#include 	"math.h"

/*
 *  calcDegree
 *  ----------
 *  calcula, para un driver determinado, la cuantos grados avanzó el motor. Esto se hace en función de los pasos avanzados.
 */
float calcDegree(a4988Handler *driver_handler) {
	return (driver_handler->stepsCount / (float) driver_handler->microstp)
			* (360 / driver_handler->stepsPerTurn);
}

void a4988_init(a4988Handler *driver_handler, GPIO_TypeDef *en_port,
		uint32_t en_pin, GPIO_TypeDef *step_port, uint32_t step_pin,
		GPIO_TypeDef *dir_port, uint32_t dir_pin) {
	driver_handler->EN_PORT = en_port;
	driver_handler->EN_PIN = en_pin;
	driver_handler->STEP_PORT = step_port;
	driver_handler->STEP_PIN = step_pin;
	driver_handler->DIR_PORT = dir_port;
	driver_handler->DIR_PIN = dir_pin;
	driver_handler->stepsCount = 0;
	driver_handler->microstp = FULL;
	driver_handler->stepDuration = 2;
	driver_handler->stepsPerTurn = 200;
	a4988_setStatus(driver_handler, OFF);
	DWT_Init();
}

bool a4988_incStep(a4988Handler *driver_handler) {
	if (driver_handler->status != ACTIVE)
		return false;
	HAL_GPIO_WritePin(driver_handler->DIR_PORT, driver_handler->DIR_PIN,
			GPIO_PIN_SET);
	DWT_Delay(1);
	HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
			GPIO_PIN_SET);
	DWT_Delay(1);
	HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
			GPIO_PIN_RESET);
	DWT_Delay(1);
	++driver_handler->stepsCount;
	return true;
}

bool a4988_incSteps(a4988Handler *driver_handler, uint32_t steps) {
	if (driver_handler->status != ACTIVE)
		return false;
	HAL_GPIO_WritePin(driver_handler->DIR_PORT, driver_handler->DIR_PIN,
			GPIO_PIN_SET);
	DWT_Delay(1);
	for (uint32_t i = 0; i < steps; ++i) {
		HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
				GPIO_PIN_SET);
		DWT_Delay(driver_handler->stepDuration);
		HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
				GPIO_PIN_RESET);
		DWT_Delay(driver_handler->stepDuration);
		++driver_handler->stepsCount;
	}
	driver_handler->degreeCount = calcDegree(driver_handler);
	return true;
}

bool a4988_decStep(a4988Handler *driver_handler) {
	if (driver_handler->status != ACTIVE)
		return false;
	HAL_GPIO_WritePin(driver_handler->DIR_PORT, driver_handler->DIR_PIN,
			GPIO_PIN_RESET);
	DWT_Delay(1);
	HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
			GPIO_PIN_SET);
	DWT_Delay(1);
	HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
			GPIO_PIN_RESET);
	DWT_Delay(1);
	--driver_handler->stepsCount;
	driver_handler->degreeCount = calcDegree(driver_handler);
	return true;
}

bool a4988_decSteps(a4988Handler *driver_handler, uint32_t steps) {
	if (driver_handler->status != ACTIVE)
		return false;
	HAL_GPIO_WritePin(driver_handler->DIR_PORT, driver_handler->DIR_PIN,
			GPIO_PIN_RESET);
	DWT_Delay(1);
	for (uint32_t i = 0; i < steps; ++i) {
		HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
				GPIO_PIN_SET);
		DWT_Delay(driver_handler->stepDuration);
		HAL_GPIO_WritePin(driver_handler->STEP_PORT, driver_handler->STEP_PIN,
				GPIO_PIN_RESET);
		DWT_Delay(driver_handler->stepDuration);
		--driver_handler->stepsCount;
	}
	driver_handler->degreeCount = calcDegree(driver_handler);
	return true;
}

void a4988_setMicroStep(a4988Handler *driver_handler, microStep step) {
	driver_handler->microstp = step;
}

void a4988_setStepsPerTurn(a4988Handler *driver_handler, float steps) {
	driver_handler->stepsPerTurn = steps;
}

float a4988_getDegreeCount(a4988Handler *driver_handler) {
	return driver_handler->degreeCount;
}

int64_t a4988_getSteps(a4988Handler *driver_handler) {
	return driver_handler->stepsCount;
}

/*
 *	a4988_setSteps
 *	--------------
 *	Asigna los steps avanzados en el motor. Con ellos se peude asignar un nuevo punto de
 *	referencia.
 */
void a4988_setSteps(a4988Handler *driver_handler, int64_t steps) {
	driver_handler->stepsCount = steps;
}

void a4988_resetSteps(a4988Handler *driver_handler) {
	driver_handler->stepsCount = 0;
}

void a4988_setStatus(a4988Handler *driver_handler, driverStatus status) {
	driver_handler->status = status;
	switch (status) {
	case ACTIVE:
		HAL_GPIO_WritePin(driver_handler->EN_PORT, driver_handler->EN_PIN,
				GPIO_PIN_RESET);
		driver_handler->status = ACTIVE;
		break;
	case OFF:
		HAL_GPIO_WritePin(driver_handler->EN_PORT, driver_handler->EN_PIN,
				GPIO_PIN_SET);
		driver_handler->status = OFF;
		break;
	default:
		break;
	}
}

void a4988_setSpeed(a4988Handler *driver_handler, uint32_t speed) {
	driver_handler->stepDuration = (uint32_t) ((2 * M_PI * 1000000)
			/ ((float) speed * (float) driver_handler->microstp
					* driver_handler->stepsPerTurn)) / 2;
}
