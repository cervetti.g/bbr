/*! @mainpage Pololu A4988 Driver library
 *
 * @section Description
 *
 * This library manages a pololu A4988 stepper motor driver. It uses structures, allowing to use
 * Different drivers in the same project.
 *
 * @section	Features
 * - Struct approach that allows to use different drivers at once.
 * - Driver can be turn on and off in order to save energy.
 * - Software step count.
 * - Speed of the motor can be set.
 *
 * @section Bugs
 * - Sleep mode not implemented.
 *
 */

/**
 *  A4988 library
 *  @file a4988.h
 *	@author Gonzalo Cervetti
 *	@date Sep 14, 2019
 *	@version 1.0
 *	@copyright GNU Public License.
 *	@brief This library manages a Pololu A4988 stepper motor driver. This library does not drive SLEEP mode.
 *
 *	@section sec Pin naming
 * 	  pin assignment <br>
 *    GPIO_TypeDef *EN_PORT = Port of ENABLE pin.			<br>
 *    uint16_t EN_PIN		= ENABLE pin.					<br>
 *    GPIO_TypeDef *STEP_PORT	= port of STEP pin.			<br>
 *    uint16_t STEP_PIN			= STEP pin.					<br>
 *    GPIO_TypeDef *DIR_P		= Port of DIRECCION pin.	<br>
 *    GPIO_TypeDef *DIR_PORT 	= DIRECCION pin.			<br>
 *
 *	@section sec2 Example code
 *	@code {.c}
 *		a4988Handler motor1;
 *		void main(){
 *			a4988_init(&motor1,A4988_EN1_GPIO_Port, A4988_EN1_Pin, A4988_STP1_GPIO_Port, A4988_STP1_Pin , A4988_DIR1_GPIO_Port, A4988_DIR1_Pin);
 *			a4988_setSpeed(&motor1, 15);
 *			a4988_setStatus(&motor1,ACTIVE);
 *			HAL_Delay(100);
 *			a4988_incSteps(&motor1, 400);
 *		}
 *	@endcode
 */

#ifndef A4988_A4988_H_
#define A4988_A4988_H_

#include "stdint.h"
#include "stdbool.h"
#include "main.h"
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_gpio.h"
#include "gpio.h"

/*!
 * @enum driverStatus
 * @brief Possible status of the driver.
 * @bug SLEEP is no implemented
 */
typedef enum
{
	ACTIVE,		///< ACTIVE state
	OFF,		///< OFF state
	SLEEP,		///< SLEEP state
} driverStatus; //Enumeración que define el estado del motor.

/*!
 * @enum microStep
 * @brief Microsteps set in the driver by pins MS1, MS2, MS3
 */
typedef enum
{
	FULL = 1,		///< FULL step (no microstepping)
	HALF = 2,		///< 1/2 of a step
	QUARTER = 4,	///< 1/4 of a step
	EIGHT = 8,		///< 1/8 of a step
	SIXTEENTH = 16, ///< 1/16 of a step
} microStep;

/*!
 * @struct a4988Handler
 * @brief this struct saves all the information related to the driver status.
 * @details In order to control multiple drivers, one struct for each of them must
 * be set. Values can be read from the struct or by the functions given.
 */
typedef struct
{
	GPIO_TypeDef *EN_PORT;   ///< Pointer to ENABLE port.
	uint16_t EN_PIN;		 ///< Reference to ENABLE pin.
	GPIO_TypeDef *STEP_PORT; ///< Pointer to STEP port.
	uint16_t STEP_PIN;		 ///< Pointer to STEP pin.
	GPIO_TypeDef *DIR_PORT;  ///< Pointer to DIR port.
	uint16_t DIR_PIN;		 ///< Pointer to DIR pin.
	float degreeCount;		 ///< Software count of the degrees that the motor has rotated since starting point.
	float stepsPerTurn;		 ///< Stores how many steps has to be sent in order to spin a turn (withouth microstepping).
	driverStatus status;	 ///< Status of the motor (ACTIVE, OFF).
	int64_t stepsCount;		 ///< Software count of how many steps has the motor has move since starting point.
	uint32_t stepDuration;   ///< Duration in us of the motor. This sets the speed of the movement.
	microStep microstp;		 ///< Enumeration that sets how many microsteps are set.
} a4988Handler;

/**
 *  @brief 	Initialize the driver. Assigns the pins for each control in order to control the motor.
 *	@param 	driver_handler pointer to struct which saves all the data.
 *	@param	en_port port of pin ENABLE.
 *	@param	en_pin pin ENABLE.
 *	@param	step_port port of pin STEP.
 *	@param	step_pin pin STEP.
 *	@param	dir_port port of pin DIRECTION.
 *	@param	dir_pin pin DIRECTION.
 *	@return void
 */
void a4988_init(a4988Handler *driver_handler, GPIO_TypeDef *en_port,
				uint32_t en_pin, GPIO_TypeDef *step_port, uint32_t step_pin,
				GPIO_TypeDef *dir_port, uint32_t dir_pin);

/**
 *  @brief 	Increments a step in the motor.
 *	@param  driver_handler Struct point to the driver to affect.
 *	@return True if no error, False if something fails.
 */
bool a4988_incStep(a4988Handler *driver_handler);

/**
 *  @brief 	Increments an specific amount of steps in the motor.
 *	@param  driver_handler Struct pointer to the driver to be affect.
*   @param  steps How many steps do you want to move.
 *	@return True if no error, False if something fails.
 */
bool a4988_incSteps(a4988Handler *driver_handler, uint32_t steps);

/**
 * @brief Decrements a simgle step in the motor.
 * @param driver_handler Struct pointer to the driver to be affect.
 * @return True if no error, False if something fails.
 */
bool a4988_decStep(a4988Handler *driver_handler);

/**
 * @brief Decrements an specific amount of steps in the motor.
 * @param driver_handler Struct pointer to the driver to be affect.
 * @param steps number of steps to be incremented.
 * @return True if no error, False if something fails.
 */
bool a4988_decSteps(a4988Handler *driver_handler, uint32_t steps);

/**
 * @brief Sets how many microsteps are set in Pololu driver
 * To check hoe many microsteps are configured, check MS1, MS2 and MS3.
 * @param driver_handler Struct pointer to the driver to be affect.
 * @param step number of microsteps to increment.
 */
void a4988_setMicroStep(a4988Handler *driver_handler, microStep step);

/**
 * @brief Set the amount of steps to make in order to rotate a turn.
 * DO NOT TAKE IN COUNT MICROSTEPS. See Stepper motor datasheet.
 * @param driver_handler driver_handler Struct pointer to the driver to be affect.
 * @param steps number of steps of the motor.
 */
void a4988_setStepsPerTurn(a4988Handler *driver_handler, float steps);

/**
 * @brief Set a new amount of steps taken.
 * @param driver_handler Struct pointer to the driver to be affect.
 * @param steps number of steps taken by the motor.
 */
void a4988_setSteps(a4988Handler *driver_handler, int64_t steps);

/**
 * @brief Get how many degrees has the motor rotated.
 * @param driver_handler Struct pointer to the driver to be affect.
 * @return number of steps tajen by the motor.
 */
float a4988_getDegreeCount(a4988Handler *driver_handler);

/**
 * @brief returns the number of steps that the motor has made.
 * @param driver_handler Struct pointer to the driver to be affect.
 * @return number of steps that the motor has made.
 */
int64_t a4988_getSteps(a4988Handler *driver_handler);

/**
 * @brief Reset the step count of the motor.
 * @param driver_handler Struct pointer to the driver to be affect.
 */
void a4988_resetSteps(a4988Handler *driver_handler);

/**
 * @brief Set the motor to ACTIVE or OFF
 * @param driver_handler Struct pointer to the driver to be affect.
 * @param status ACTIVE or OFF.
 */
void a4988_setStatus(a4988Handler *driver_handler, driverStatus status);

/**
 * @brief Calculates the delay of each step in order to reach the desired speed.
 * In order to calculate it properly, microsteps and steps per turn must be set.
 * @param driver_handler Struct pointer to the driver to be affect.
 * @param speed Desired speed of the motor in rads/seg.
 */
void a4988_setSpeed(a4988Handler *driver_handler, uint32_t speed);

#endif /* A4988_A4988_H_ */
