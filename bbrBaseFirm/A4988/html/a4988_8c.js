var a4988_8c =
[
    [ "a4988_decStep", "a4988_8c.html#a25f9bb9576d05153c805e499b5ead280", null ],
    [ "a4988_decSteps", "a4988_8c.html#a694a6c6580c3e70029ecf890b18662d9", null ],
    [ "a4988_getDegreeCount", "a4988_8c.html#a05f902e3d1ee0b82d70fa65d8955423b", null ],
    [ "a4988_getSteps", "a4988_8c.html#ab2c561e95b718d8f275f41f7ec44113b", null ],
    [ "a4988_incStep", "a4988_8c.html#a6cd8fcd5ab73faea97783466d496782b", null ],
    [ "a4988_incSteps", "a4988_8c.html#a616386d44c4d6ab3acd4bfe79f4526c3", null ],
    [ "a4988_init", "a4988_8c.html#a83a5e4ad4b743e7a5e5e3667d5b5af90", null ],
    [ "a4988_resetSteps", "a4988_8c.html#a4636e615969f0762717be85f3fae99b2", null ],
    [ "a4988_setMicroStep", "a4988_8c.html#a50b770ddd61a733c0b93674a6e043941", null ],
    [ "a4988_setSpeed", "a4988_8c.html#a957c1d38216e19e2d33514fba8290797", null ],
    [ "a4988_setStatus", "a4988_8c.html#aac1c631bf4552cbc8e1af42cf426f154", null ],
    [ "a4988_setSteps", "a4988_8c.html#a8c6e8d00fb33d5b8d8b1be976b2d8773", null ],
    [ "a4988_setStepsPerTurn", "a4988_8c.html#a2c4fb12bcdcffea40744538616b5f7e4", null ],
    [ "calcDegree", "a4988_8c.html#a87da3e1c65c00623174d77c27ca6b712", null ]
];