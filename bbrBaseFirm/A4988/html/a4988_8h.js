var a4988_8h =
[
    [ "a4988Handler", "structa4988_handler.html", "structa4988_handler" ],
    [ "driverStatus", "a4988_8h.html#a6086cd48f9d99d3c531787235d509622", [
      [ "ACTIVE", "a4988_8h.html#a6086cd48f9d99d3c531787235d509622a33cf1d8ef1d06ee698a7fabf40eb3a7f", null ],
      [ "OFF", "a4988_8h.html#a6086cd48f9d99d3c531787235d509622aac132f2982b98bcaa3445e535a03ff75", null ],
      [ "SLEEP", "a4988_8h.html#a6086cd48f9d99d3c531787235d509622ad6137abebe4fdc59e2f0f2c84bdbe3fa", null ]
    ] ],
    [ "microStep", "a4988_8h.html#a26a4a9fc10f890a34859f53d114f08b0", [
      [ "FULL", "a4988_8h.html#a26a4a9fc10f890a34859f53d114f08b0ad08f8ac0aa8dfb59589824359772459e", null ],
      [ "HALF", "a4988_8h.html#a26a4a9fc10f890a34859f53d114f08b0a51df22c376649e022134d738e8d8641a", null ],
      [ "QUARTER", "a4988_8h.html#a26a4a9fc10f890a34859f53d114f08b0a8dd9342a0e4fd716d2beb9961f5deeed", null ],
      [ "EIGHT", "a4988_8h.html#a26a4a9fc10f890a34859f53d114f08b0a2fd39b67b424f4ea3b4c11ce3195ab8b", null ],
      [ "SIXTEENTH", "a4988_8h.html#a26a4a9fc10f890a34859f53d114f08b0a3cc9428274f66ec477863bbf676438c8", null ]
    ] ],
    [ "a4988_decStep", "a4988_8h.html#a25f9bb9576d05153c805e499b5ead280", null ],
    [ "a4988_decSteps", "a4988_8h.html#a694a6c6580c3e70029ecf890b18662d9", null ],
    [ "a4988_getDegreeCount", "a4988_8h.html#a05f902e3d1ee0b82d70fa65d8955423b", null ],
    [ "a4988_getSteps", "a4988_8h.html#ab2c561e95b718d8f275f41f7ec44113b", null ],
    [ "a4988_incStep", "a4988_8h.html#a6cd8fcd5ab73faea97783466d496782b", null ],
    [ "a4988_incSteps", "a4988_8h.html#a616386d44c4d6ab3acd4bfe79f4526c3", null ],
    [ "a4988_init", "a4988_8h.html#a83a5e4ad4b743e7a5e5e3667d5b5af90", null ],
    [ "a4988_resetSteps", "a4988_8h.html#a4636e615969f0762717be85f3fae99b2", null ],
    [ "a4988_setMicroStep", "a4988_8h.html#a50b770ddd61a733c0b93674a6e043941", null ],
    [ "a4988_setSpeed", "a4988_8h.html#a957c1d38216e19e2d33514fba8290797", null ],
    [ "a4988_setStatus", "a4988_8h.html#aac1c631bf4552cbc8e1af42cf426f154", null ],
    [ "a4988_setSteps", "a4988_8h.html#a8c6e8d00fb33d5b8d8b1be976b2d8773", null ],
    [ "a4988_setStepsPerTurn", "a4988_8h.html#a2c4fb12bcdcffea40744538616b5f7e4", null ]
];