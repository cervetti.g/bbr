var structa4988_handler =
[
    [ "degreeCount", "structa4988_handler.html#a36a117294bb9f01149cd2b3fb56e3195", null ],
    [ "DIR_PIN", "structa4988_handler.html#a8ae7c243b84a9b7239062b46831d3a2c", null ],
    [ "DIR_PORT", "structa4988_handler.html#ad6b16cf6efd58235600d7fe073f9f4e3", null ],
    [ "EN_PIN", "structa4988_handler.html#a464ea2ca2566af32ddd2be6c597aa2ad", null ],
    [ "EN_PORT", "structa4988_handler.html#ad58bb9d10d11d874bb55865bb8a7ca2a", null ],
    [ "microstp", "structa4988_handler.html#a389a2f0528c7e35a513aacbc67ea62b6", null ],
    [ "status", "structa4988_handler.html#ad51f96267735fd1c81b0bb63530eeb09", null ],
    [ "STEP_PIN", "structa4988_handler.html#ad208d8cc0de2201171b95649d744a2b9", null ],
    [ "STEP_PORT", "structa4988_handler.html#a94941df86d5999aec0219c6bfe4f42b2", null ],
    [ "stepDuration", "structa4988_handler.html#af49728ffceff31645ba020df66f2804d", null ],
    [ "stepsCount", "structa4988_handler.html#aa6419d0080b8d90e315600a51cafbd50", null ],
    [ "stepsPerTurn", "structa4988_handler.html#a64fef8e0e1a8071b1ba817e3d38dc6e9", null ]
];