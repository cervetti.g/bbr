var searchData=
[
  ['sixteenth',['SIXTEENTH',['../a4988_8h.html#a26a4a9fc10f890a34859f53d114f08b0a3cc9428274f66ec477863bbf676438c8',1,'a4988.h']]],
  ['sleep',['SLEEP',['../a4988_8h.html#a6086cd48f9d99d3c531787235d509622ad6137abebe4fdc59e2f0f2c84bdbe3fa',1,'a4988.h']]],
  ['status',['status',['../structa4988_handler.html#ad51f96267735fd1c81b0bb63530eeb09',1,'a4988Handler']]],
  ['step_5fpin',['STEP_PIN',['../structa4988_handler.html#ad208d8cc0de2201171b95649d744a2b9',1,'a4988Handler']]],
  ['step_5fport',['STEP_PORT',['../structa4988_handler.html#a94941df86d5999aec0219c6bfe4f42b2',1,'a4988Handler']]],
  ['stepduration',['stepDuration',['../structa4988_handler.html#af49728ffceff31645ba020df66f2804d',1,'a4988Handler']]],
  ['stepscount',['stepsCount',['../structa4988_handler.html#aa6419d0080b8d90e315600a51cafbd50',1,'a4988Handler']]],
  ['stepsperturn',['stepsPerTurn',['../structa4988_handler.html#a64fef8e0e1a8071b1ba817e3d38dc6e9',1,'a4988Handler']]]
];
