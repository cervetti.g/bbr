%This script emulates the mathematics that the robot has to do 
%in order to calculate the speed of each motor to move in x-y direction.
%Radius of the ball 
rb = 0.10;
%Radius of the wheel
rw = 0.03;

% Desired speeds in x,y and wbz.

v = [ 0.01;
      0.0;
      0.0]


theta = (2 / 9) * pi();

rMat = [0, -1, 0 ;
        1,  0, 0 ;
        0,  0, -rb];
        
%transformation matrix
wMat =[               0,                    cos(theta),               -sin(theta);
        -((sqrt(3) / 2)) * cos(theta),   (-(1 / 2)) * cos(theta),     -sin(theta);
         ((sqrt(3) / 2)) * cos(theta)    (-(1 / 2)) * cos(theta),     -sin(theta)];                                                  
rSwMat = (-1/rw) * wMat
rMrtMat =  rSwMat * (rMat')  
rrtMrMat = rMrtMat * rMat       
result =  rrtMrMat * v